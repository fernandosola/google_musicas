# Google Músicas #

Bot para buscar o ano de musicas consultando pelo nome da música e artista.

A idéia básica é fazer uma pesquisa no google e identificar o quadro específico que o google exibe quando possui as informações detalhadas da música.

Normalmente as informações retornadas pela pesquisa são: 
```
#!text
Artista: Led Zeppelin
Filme: The Song Remains the Same
Data de lançamento: 1971
Prêmios: Grammy Hall of Fame Award
Gênero: Hard rock
```

## Problemas identificados:

Para as duas estratégias, o google limita o número de chamadas ao identificar que é um robô que está realizando as consultas.

## Estratégia para resolução

1. Descobrir a url correta que é chamada pelo google durante a pesquisa e fazer o parsing do fonte ou utilizar alguma API de testes para fazer as chamadas através de um browser simulando um usuário.

2. Para realizar a chamada é necessário utilizar diversos proxies de navegação anônima para que cada consulta tenha um ip diferente. A classe Bot foi implementada para utilizar esta abordagem, fazendo o uso do selenium e navegando pela url solicitada através de proxies pré-definidos.


## Setup do ambiente para uso (Python3)

1. Instalar o navegador Ópera;
2. Instalar o Selenium conforme a documentação e o WebDriver para o Ópera;
```
http://selenium-python.readthedocs.io/installation.html
```
3. Instalar as bibliotecas:
```
pip3 install xlrd
pip3 install pandas
pip3 install selenium
```

* Versão: 0.1